import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColegioServiceService {

  api: string;


  constructor(private http: HttpClient) {
    this.api = environment.protocol + environment.host + environment.apiUrl;
  }

  public consultarInfoHome(): Observable<any> {
    return this.http.get(this.api + '/consultarCursos/1', {
      observe: 'response'
    });
  }

  public consultarDetalle(idCurso: any): Observable<any> {
    return this.http.get(this.api + '/consultaDetalle/' + idCurso, {
      observe: 'response'
    });
  }

  public consultarTodasAsignaturas(): Observable<any> {
    return this.http.get(this.api + '/consultarTodasAsignaturas', {
      observe: 'response'
    });
  }

  public consultarTodosProfesores(): Observable<any> {
    return this.http.get(this.api + '/consultarTodosProfesores', {
      observe: 'response'
    });
  }

}
