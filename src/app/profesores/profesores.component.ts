import { Component, OnInit } from '@angular/core';
import { ColegioServiceService } from 'src/app/services/colegio-service.service';

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styleUrls: ['./profesores.component.css']
})
export class ProfesoresComponent implements OnInit {

  public datosResultado: any = [];
  public showMensajeError: any = false;
  public showEstudiantes: any = false;
  public datosEstudiantes: any = {};
  public nombreProfesor: any = null;

  constructor(private colegioService: ColegioServiceService) { }

  ngOnInit(): void {
    this.consultarProfesores();
  }

  public consultarProfesores() {
    this.colegioService.consultarTodosProfesores().subscribe((data) => {
      if (data.status === 200) {
        this.datosResultado = data.body;
        if (this.datosResultado.codigoResultado !== 'EXITOSO') {
          this.showMensajeError = true;
        }
      }
    });
  }

  public detalleEstudiantes(item: any, nombreProfesor: any) {
    this.showEstudiantes = true;
    this.datosEstudiantes = item;
    this.nombreProfesor = nombreProfesor;
  }

  public regresar() {
    this.showEstudiantes = false;
  }

}
