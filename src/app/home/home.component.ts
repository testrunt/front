import { Component, OnInit } from '@angular/core';
import { ColegioServiceService } from 'src/app/services/colegio-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public resultadoConsulta: any = {};
  public mensajeError: any = null;
  public showMensajeError: any = false;
  public datosDetalle: any = {};
  public showDetalle: any = false;

  constructor(private colegioService: ColegioServiceService) { }

  ngOnInit(): void {
    this.consultarInfoHome();
  }

  public consultarInfoHome() {
    this.colegioService.consultarInfoHome().subscribe((data) => {
      if (data.status === 200) {
        const datosResultado = data.body;
        if (datosResultado.codigoResultado === 'EXITOSO') {
          this.resultadoConsulta = datosResultado.salida;
          this.consultarDetalle(1);
        } else {
          this.mensajeError = this.resultadoConsulta.mensajeSalida;
          this.showMensajeError = true;
        }
      }
    });
  }

  public consultarDetalle(idCurso: any) {
    this.colegioService.consultarDetalle(idCurso).subscribe((data) => {
      if (data.status === 200) {
        const datosResultado = data.body;
        if (datosResultado.codigoResultado === 'EXITOSO') {
          this.datosDetalle = datosResultado.salida;
        } else {
          this.mensajeError = this.resultadoConsulta.mensajeSalida;
          this.showDetalle = true;
        }
      }
    });
  }

}
