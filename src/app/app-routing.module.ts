import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfesoresComponent } from './profesores/profesores.component';

const routes: Routes = [

  { path: "home", component: HomeComponent },
  { path: "profesores", component: ProfesoresComponent },

  { path: "**", pathMatch: "full", redirectTo: "home" }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
